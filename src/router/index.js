import VueRouter from 'vue-router'
import Vue from 'vue'
import Login from '@/views/Login'
import Main from '@/views/Main'
import AllMessage from '@/views/ChatBoxMenu/AllMessage'
import store from './../store'

Vue.use(VueRouter)

const routes = [
  {
    path: '',
    // redirect重定向
    redirect: '/main'
  },
  {
    name: "login",
    path: "/login",
    component: Login
  }, {
    path: '/main',
    name: 'main',
    component: Main,
    children: [
      {
        path: '/main/allMessage',
        name: 'allMessage',
        components: { // key => value
          leftMenu: AllMessage,
      }
  }
]
  },
]
const router = new VueRouter({
  mode: "history",
  routes
});
router.beforeEach((to, from, next) => {
  /**tips:需要在钩子函数内读取登录状态 */
  const userIsLogin = store.state.user.isLogin
  if (to.meta.requiresAuth) {
    if (userIsLogin) {
      next()
    } else {
      // alert('请先登录再进行此操作!')
      next({
        path: '/login',
        /** 将刚刚要去的路由path（却无权限）作为参数，方便登录成功后直接跳转到该路由 */
        query: { redirect: to.fullPath }
      })
    }
  } else {
    next()
  }
})

// 将router对象传入到Vue实例
export default router
