import {post} from "@/utils/httpRes";

// 登录请求
export function login(data) {
  return post("/login", data);
}