import axios from "axios"
import {Message} from "element-ui";
import router from './../router'
import {getToken, setToken, removeToken} from './token'

axios.interceptors.request.use(
    config => {
      config.timeout = 700000;
      config.baseURL = '/';
      const token = getToken()
      if (token) {
        config.headers.Authorization = token
      }
      return config
    },
    err => {
      Promise.reject(err)
    }
)
// 成功：2000，失败（无数据）：2001，未登录：2002，服务端错误：2003
// 登录成功：1000，登录失败（账号或密码错误）：1001，验证码错误：1002
// 用户已被注册:1003,注册失败:1004,注册成功:1005,用户验证过期：1006
// 验证码过期：1007，非法登录（token无效，解析异常）：1009
axios.interceptors.response.use(
    response => {
      console.log(response);
      if (response.data.status === 2000) {
        setToken(response.data.data.token)
      }
      if (response.data.code === 2002) { // 未登录
        Message({
          message: '请先登录',
          type: 'warning',
          duration: 3000
        })
        router.push('/login')
      } else if (response.data.code === 2003) {
        Message({
          message: '服务端错误，请稍后重试',
          type: 'error',
          duration: 3000
        })
      } else if (response.data.code === 1006) {
        Message({
          message: '登录过期',
          type: 'warning',
          duration: 3000
        })
        removeToken()
        router.push('/login')
      } else if (response.data.code === 1007) {
        Message({
          message: '验证码过期！',
          type: 'warning',
          duration: 3000
        })
      } else if (response.data.code === 1009) {
        Message({
          message: '非法登录，请注意规范！',
          type: 'error',
          duration: 3000
        })
      }
      return response
    },
    error => {
      console.log('请求错误', error)
      Message({
        message: '网络超时！',
        type: 'error',
        duration: 3000
      })
      return Promise.reject(error)
    }
)

export function get(url, params) {
  return axios({
    url,
    params,
    method: "get",
  })
}

export function post(url, data) {
  return axios({
    url,
    data,
    method: "post",
  })
}

export function put(url, data) {
  return axios({
    url,
    data,
    method: "put"
  })
}

/**
 * 文件上传
 * @param url
 * @param data
 * @returns {Promise<any>}
 */
export function fileUpload(url, data) {
  return axios({
    url,
    data,
    headers: {
      'Content-Type': 'undefined',
    },
    methods: "post"
  })
}

/**
 * 封装文件下载post请求
 * @param url
 * @param data
 * @returns {Promise}
 */
/*export function downloadPost (url,data){
  return axios({
    url,
    data,
    responseType: 'blob',
    methods: "post"
  })
}*/

export function downloadPost (url, data = {}){
  return new Promise((resolve,reject) => {
    axios.post(url, data,{
      responseType: 'blob'
    })
        .then(response => {
          resolve(response);
        }).catch(err=>{
      reject(err);
    });
  });
}

