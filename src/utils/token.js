import Cookies from 'js-cookie'

const authorization = "authorization"

export const setToken = (token) => {
  Cookies.set(authorization, token)
}

export const getToken = () => {
  return Cookies.get(authorization)
}

export const removeToken = () => {
  return Cookies.remove(authorization)
}