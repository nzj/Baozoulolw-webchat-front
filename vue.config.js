module.exports = {
  outputDir: "dist",
  filenameHashing: false,
  productionSourceMap: false,
  devServer: {proxy: "http://localhost:8001"},
  configureWebpack: {performance: {hints: false}}
};